import { User } from 'src/app/_models/user';
import { AuthService } from 'src/app/_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  user: any;
  jwtHelper = new JwtHelperService();
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.user = this.jwtHelper.decodeToken(localStorage.getItem('token'));
  }

  logout(): void {
    this.authService.logout();
  }

}
