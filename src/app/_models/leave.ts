export interface Leave {
    id: number;
    reason: string;
    fromDate: Date;
    toDate: Date;
    userId: number;
    status: string;
    type: string;
    project: string;
    pm: number;
}


