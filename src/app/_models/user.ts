export interface User {
  id: number;
  email: string;
  age: number;
  gender: string;
  country: string;
  role: string;
}
