import { AuthService } from 'src/app/_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/_services/alert-service.service';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  onSubmit(): void {
    this.authService.login(this.loginForm.value).pipe(
      mergeMap(() => this.authService.getCurrentUser())
    ).subscribe(
      u => localStorage.setItem('role', u.role),
      err => this.alertService.error(err.error),
      () => this.router.navigate([this.returnUrl])
    );
  }
  // onSubmit(): void {
  //   this.authService.login(this.loginForm.value).pipe(
  //     mergeMap(() => this.authService.getCurrentUser())
  //   ).subscribe(
  //     u => localStorage.setItem('role', u.role),
  //     err => this.alertService.error(err.error),
  //     () => this.router.navigate([this.returnUrl])
  //   );
  // }
}
