import { AlertService } from './../../_services/alert-service.service';
import { Router } from '@angular/router';
import { AuthService } from './../../_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: User;
  registerForm: FormGroup;
  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private route: Router,
              private alertService: AlertService) { }

  ngOnInit(): void {
    this.createRegisterForm();
  }

  createRegisterForm(): void {
    this.registerForm = this.fb.group({
      gender: ['', Validators.required],
      age: ['', [Validators.required, Validators.max(100), Validators.min(10)]],
      country: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(4)]],
      confirmPassword: ['', Validators.required]
    }, { validators: this.checkPassword });
  }

  checkPassword(fg: FormGroup): {mismatch: true} {
    return fg.get('password').value === fg.get('confirmPassword').value ? null : { mismatch: true };
  }

  register(): void {
    if (this.registerForm.valid) {
      this.user = Object.assign({}, this.registerForm.value);
      this.user.role = 'SE';
      this.authService.register(this.user).subscribe(() => {
        this.alertService.success('register successful', {autoClose: true, keepAfterRouteChange: true});
      }, error => {
        this.alertService.error(error.error,  {autoClose: true, keepAfterRouteChange: true});
        console.log(error.error);
      }, () => {
        this.authService.login(this.user).subscribe(() => {
          this.route.navigate(['/']);
        });
      });
    }
    console.log(this.registerForm.value);
  }

}
