import { UserService } from './../../_services/user.service';
import { User } from './../../_models/user';
import { AlertService } from './../../_services/alert-service.service';
import { AuthService } from './../../_services/auth.service';
import { LeaveService } from './../../_services/leave.service';
import { Leave } from './../../_models/leave';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LeaveModalComponent } from '../leave-modal/leave-modal.component';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-leaverequests',
  templateUrl: './leaverequests.component.html',
  styleUrls: ['./leaverequests.component.scss']
})
export class LeaverequestsComponent implements OnInit {

  leaveRequests: any[];
  leaveToView: Leave;
  bsModalRef: BsModalRef;
  users: User[];
  constructor(
    private leaveService: LeaveService,
    private authService: AuthService,
    private alertService: AlertService,
    private modalService: BsModalService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    const leaves = this.leaveService.getLeaveRequestByPmId(this.authService.getCurrentUserid());
    const users = this.userService.getAllUsers();
    forkJoin([leaves, users]).subscribe(result => {
      this.leaveRequests = result[0],
        this.users = result[1];
      },
        error =>
      this.alertService.error(error.error, { autoClode: true }),
      () => {
        this.leaveRequests.forEach(el => {
          // tslint:disable-next-line: triple-equals
          const maptoUser = this.users.find(x => x.id == el.userId);
          el.userEmail = maptoUser.email;
        });
      });
  }

  viewLeave(leaveToView: Leave): void {
    const initialState = {
      leave: leaveToView,
      title: 'Leave Request'
    };
    this.bsModalRef = this.modalService.show(LeaveModalComponent, { initialState });
    this.bsModalRef.content.updateLeaveToApprove.subscribe((value) => {
      const leaveToUpdate = value;
      leaveToUpdate.status = 'Approval';
      this.leaveService.updateLeave(leaveToUpdate.id, leaveToUpdate).subscribe(
        data => {
          this.alertService.success('Update successful', { keepAfterRouteChange: true, autoClose: true });

        },
        error => {
          this.alertService.error(error);
        });
    });
  }
}
