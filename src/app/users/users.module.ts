import { LeaveModalComponent } from './leave-modal/leave-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersRoutingModule } from './user-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { LayoutComponent } from './layout/layout.component';
import { LeaverequestsComponent } from './leaverequests/leaverequests.component';
import { ModalModule } from 'ngx-bootstrap/modal';
@NgModule({
  declarations: [
    ListComponent,
    LayoutComponent,
    LeaverequestsComponent,
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  entryComponents: [
    LeaveModalComponent
  ]
})
export class UsersModule { }
