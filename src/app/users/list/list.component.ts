import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/_services/alert-service.service';
import { UserService } from './../../_services/user.service';

const roles = ['PM', 'SE'];
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  form: FormGroup;
  users: any;
  userToUpdate: any;
  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
  ) { }


  get formArray(): FormArray {
    return this.form.get('rowFrom') as FormArray;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      rowFrom: this.fb.array([])
    });
    this.userService.getAllUsers().subscribe(users => {
      this.users = users;
    },
      error => this.alertService.error(error), () => {
        this.users.forEach(user => {
          user.editbale = false;
        });
        this.fillRowForm(this.users);
      });

    console.log(this.form);
  }

  addForm(): void {
    const control = this.form.controls.rowFrom as FormArray;
    control.push(
      this.fb.group({
        email: this.fb.control('', [Validators.required, Validators.email]),
        id: this.fb.control('', Validators.required),
        gender: this.fb.control('', Validators.required),
        country: this.fb.control('', Validators.required),
        role: this.fb.control('', Validators.required),
        confirmPassword: this.fb.control('', Validators.required),
        editbale: this.fb.control(''),
      })
    );
  }

  fillRowForm(formList): void {
    console.log(formList);
    for (let i = 0; i < formList.length; i++) {
      if (this.formArray.length < formList.length) {
        this.addForm();
      }

      this.formArray.at(i).patchValue({
        email: formList[i].email,
        id: formList[i].id,
        gender: formList[i].gender,
        country: formList[i].country,
        role: formList[i].role,
        editbale: formList[i].editbale,
        confirmPassword: formList[i].confirmPassword,
      });
    }
  }

  editMode(data: FormGroup): void {
    data.controls.editbale.setValue(!data.controls.editbale.value);
  }

  cancel(data: FormGroup): void {
    const initValue = data.value;
    console.log(initValue);
  }


  saveUser(data: FormGroup): void {
    this.userToUpdate = data.value;
    this.userToUpdate.password = data.value.confirmPassword;
    console.log(this.userToUpdate);
    this.userService.upddateUser(this.userToUpdate.id, this.userToUpdate).subscribe(() => {    },
    error => {
      this.alertService.error(error.error, { autoClose: true });
    }, () => {
      this.alertService.success('Update user successfully', { autoClose: true });
      data.controls.editbale.setValue(false);
    });
  }
}
