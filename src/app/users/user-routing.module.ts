import { LeaverequestsComponent } from './leaverequests/leaverequests.component';
import { AuthGuard } from './../_helper/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Admin', 'PM'] },
    children: [
      { path: '', component: ListComponent,  },
      { path: 'leaverequest', component: LeaverequestsComponent,  },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
