import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertService } from 'src/app/_services/alert-service.service';
import { LeaveService } from 'src/app/_services/leave.service';
import { Leave } from './../../_models/leave';
@Injectable()
@Component({
  selector: 'app-leave-modal',
  templateUrl: './leave-modal.component.html',
  styleUrls: ['./leave-modal.component.scss']
})
export class LeaveModalComponent implements OnInit {
  @Output() updateLeaveToApprove = new EventEmitter();
  title: string;
  closeBtnName: string;
  leave: any;
  constructor(
    public bsModalRef: BsModalRef,
    private leaveService: LeaveService,
    private alertService: AlertService,
    private modalService: BsModalService
    ) { }

  ngOnInit(): void {
  }

  updateLeave(): void {
    this.updateLeaveToApprove.emit(this.leave);
    this.bsModalRef.hide();
  }
}
