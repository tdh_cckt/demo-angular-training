import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../_models/user';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtHelper = new JwtHelperService();
  constructor(private http: HttpClient, private router: Router) { }

  login(model: any): Observable<any> {
    return this.http.post(environment.baseUrl + 'login', model).pipe(
      map((response: any) => {
        if (response) {
          localStorage.setItem('token', response.accessToken);
        }
      })
    );
  }

  // tslint:disable-next-line: typedef
  register(user: User) {
    return this.http.post(environment.baseUrl + 'register', user);
  }

  loggedIn(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.router.navigate(['/account/login']);
  }

  getCurrentUserid(): number {
    const token = localStorage.getItem('token');
    return this.jwtHelper.decodeToken(token).sub;
  }

  getCurrentUser(): Observable<User> {
    return this.http.get<User>(environment.baseUrl +  'users/' +  this.getCurrentUserid());
  }

  getCurrentRole(): string {
    return localStorage.getItem('role');
  }

  roleMatch(allowRoles): boolean {
    let isMatch = false;
    const userRoles = localStorage.getItem('role');
    if (allowRoles.indexOf(userRoles) >= 0) {
      isMatch = true;
    }
    return isMatch;
  }
}
