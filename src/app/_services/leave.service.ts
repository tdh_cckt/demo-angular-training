import { Observable } from 'rxjs';
import { Leave } from './../_models/leave';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LeaveService {

  constructor(private http: HttpClient) { }

  getLeavesByUserId(id: number): Observable<Leave[]> {
    return this.http.get<Leave[]>(environment.baseUrl + 'leaves?userId=' + id);
  }
  getAllLeaves(): Observable<Leave[]> {
    return this.http.get<Leave[]>(environment.baseUrl + 'leaves');
  }

  createLeave(leave: Leave): Observable<Leave> {
    return this.http.post<Leave>(environment.baseUrl + 'leaves', leave);
  }

  getById(id: string): Observable<Leave> {
    return this.http.get<Leave>(environment.baseUrl + 'leaves/' + id);
  }

  // tslint:disable-next-line: typedef
  updateLeave(id, params) {
    return this.http.put(environment.baseUrl + 'leaves/' + id, params);
  }

  deleteLeave(id: number): Observable<Leave> {
    return this.http.delete<Leave>(environment.baseUrl + 'leaves/' + id);
  }

  getLeaveRequestByPmId(id: number): Observable<Leave[]> {
    return this.http.get<Leave[]>(environment.baseUrl + 'leaves?pm=' + id);
  }
}
