import { User } from 'src/app/_models/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  jwtHelper = new JwtHelperService();
  constructor(private http: HttpClient, private router: Router) { }

  getAllUsers(): Observable<User[]>{
    return this.http.get<User[]>(environment.baseUrl + 'users');
  }

  // tslint:disable-next-line: typedef
  upddateUser(id, params) {
    return this.http.put(environment.baseUrl + 'users/' + id, params);
  }

  getAllPMs(): Observable<User[]> {
    return this.http.get<User[]>(environment.baseUrl + 'users?role=PM');
  }
}
