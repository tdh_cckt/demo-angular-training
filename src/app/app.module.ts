import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LeavesModule } from './leaves/leaves.module';
import { UsersModule } from './users/users.module';
import { AlertComponent } from './_components/alert/alert.component';
import { NavComponent } from './_components/nav/nav.component';
import { HasRoleDirective } from './_directives/hasRole.directive';
import { AuthService } from './_services/auth.service';
@NgModule({
  declarations: [
    AppComponent,
      HomeComponent,
      AlertComponent,
      NavComponent,
      HasRoleDirective,
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    UsersModule,
    LeavesModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
  ],
  providers: [
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
