import { AddEditLeaveComponent } from './add-edit-leave/add-edit-leave.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveRoutingModule } from './leave-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { LeaveListComponent } from './leaveList/leaveList.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [
    LayoutComponent,
    LeaveListComponent,
    AddEditLeaveComponent
  ],
  imports: [
    CommonModule,
    LeaveRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
  ]
})
export class LeavesModule { }
