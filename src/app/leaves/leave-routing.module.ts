import { LeaveListComponent } from './leaveList/leaveList.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AddEditLeaveComponent } from './add-edit-leave/add-edit-leave.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            {
                path: '', component: LeaveListComponent
            },
            {
                path: 'add', component: AddEditLeaveComponent
            },
            {
                path: 'edit/:id', component: AddEditLeaveComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LeaveRoutingModule { }
