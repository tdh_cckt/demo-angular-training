import { AuthService } from 'src/app/_services/auth.service';
import { Leave } from './../../_models/leave';
import { LeaveService } from './../../_services/leave.service';
import { AlertService } from 'src/app/_services/alert-service.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-leaveList',
  templateUrl: './leaveList.component.html',
  styleUrls: ['./leaveList.component.scss']
})
export class LeaveListComponent implements OnInit {

  constructor(
    private leaveService: LeaveService,
    private alertService: AlertService,
    private authService: AuthService) { }
  leaves: Leave[];
  ngOnInit(): void {
    this.leaveService.getLeavesByUserId(this.authService.getCurrentUserid()).pipe(first()).subscribe(lev => {
      this.leaves = lev;
      console.log(this.leaves);
    },
      error => this.alertService.error(error));
  }

  deleteLeave(id: number): void {
    const user = this.leaves.find(x => x.id === id);
    this.leaveService.deleteLeave(id)
      .pipe(first())
      .subscribe(() => {
        this.leaves = this.leaves.filter(x => x.id !== id);
      });
  }
}
