import { UserService } from './../../_services/user.service';
import { User } from './../../_models/user';
import { AlertService } from 'src/app/_services/alert-service.service';
import { Leave } from './../../_models/leave';
import { AuthService } from 'src/app/_services/auth.service';
import { LeaveService } from './../../_services/leave.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-add-edit-leave',
  templateUrl: './add-edit-leave.component.html',
  styleUrls: ['./add-edit-leave.component.scss']
})
export class AddEditLeaveComponent implements OnInit {
  form: FormGroup;
  leave: Leave;
  id: string;
  isAddMode: boolean;
  leaveTypes = ['Annual Leave SD', 'Carried Over', 'Personal Leave', 'Unpaid Leave'];
  pms = [];
  minDate: Date;
  constructor(
    private fb: FormBuilder,
    private leaveService: LeaveService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private router: Router,
    private userService: UserService
  ) {
    this.minDate = new Date();
    console.log(this.minDate);
   }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;

    this.getPMs();

    this.createForm();

    if (!this.isAddMode) {
      this.editModeForm();
    }
  }

  getPMs(): void {
    this.userService.getAllPMs().subscribe(x => {
      x.forEach(element => {
        this.pms.push(element);
      });
    });
  }

  createForm(): void {
    this.form = this.fb.group({
      reason: ['', Validators.required],
      fromDate: ['', Validators.required],
      toDate: ['', Validators.required],
      type: ['', Validators.required],
      project: ['', Validators.required],
      pm: ['', Validators.required],
    }, { validators: this.isToDateHigherFromDate});
  }

  isToDateHigherFromDate(fg: FormGroup): {higherFormDate: true} {
    return fg.get('toDate').value < fg.get('fromDate').value ? { higherFormDate: true } : null;
  }

  editModeForm(): void {
    this.leaveService.getById(this.id)
      .pipe(first())
      .subscribe(x => {
        this.form.controls.reason.setValue(x.reason),
          this.form.controls.fromDate.setValue(x.fromDate),
          this.form.controls.toDate.setValue(x.toDate),
          this.form.controls.type.setValue(x.type),
          this.form.controls.project.setValue(x.project),
          this.form.controls.pm.setValue(x.pm),
          this.leave = x;
        console.log(this.leave);
        if (this.leave.status === 'Approval') {
          this.alertService.error('Dont try to delelte Approval leave request', { autoClose: true, keepAfterRouteChange: true });
          this.router.navigate(['leaves']);
        }
      });
  }

  onSubmit(): void {
    this.leave = this.form.value;
    this.leave.userId = this.authService.getCurrentUserid();
    console.log(this.form);

    if (this.form.invalid) {
      this.validateAllFormFields(this.form);
      return;
    }
    if (this.isAddMode) {
      this.createLeave();
    } else {
      this.updateLeave();
    }
  }

  validateAllFormFields(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  // tslint:disable-next-line: typedef
  private createLeave() {
    this.leave.status = 'Pending';
    this.leaveService.createLeave(this.leave)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Created successfully', { keepAfterRouteChange: true, autoClose: true });
          this.router.navigate(['.', { relativeTo: this.route }]);
        },
        error => {
          this.alertService.error(error);
        });
  }
  private updateLeave(): void {
    this.leaveService.updateLeave(this.id, this.form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Update successful', { keepAfterRouteChange: true, autoClose: true });
          this.router.navigate(['..', { relativeTo: this.route }]);
        },
        error => {
          this.alertService.error(error);
        });
  }
}
