import { AlertService } from './../_services/alert-service.service';
import { AuthService } from 'src/app/_services/auth.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const roles = route.data.roles as Array<string>;
    if (roles) {
      const match = this.authService.roleMatch(roles);
      if (match) {
        return true;
      } else {
        this.authService.logout();
        this.alertService.error('You are authorised to acces this page', {autoClose: true, keepAfterRouteChange: true});
      }
    }

    if (this.authService.loggedIn()) {
      return true;
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
    }
    this.router.navigate(['/account/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
