import { AuthService } from 'src/app/_services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'demo';
  constructor(private authService: AuthService) {

  }
  isLoggin(): boolean {
    return this.authService.loggedIn();
  }
}
